Clone repo, first `xcode-select --install`


# Dotfiles

## dotbot

More info: https://github.com/anishathalye/dotbot

```sh-session
exa ~/Bootstrap -all --long --icons --no-user --tree --level=2
```

```
drwxr-xr-x    -  9 Feb 14:47  ~/Bootstrap
drwxr-xr-x    -  9 Feb 14:47 ├──  .git
drwxr-xr-x    -  8 Feb 22:30 │  ├──  hooks
drwxr-xr-x    -  8 Feb 22:30 │  ├──  info
drwxr-xr-x    -  9 Feb 11:53 │  ├──  logs
drwxr-xr-x    -  9 Feb 14:48 │  ├──  objects
drwxr-xr-x    -  9 Feb 11:12 │  ├──  refs
.rw-r--r--   41  9 Feb 12:10 │  ├──  AUTO_MERGE
.rw-r--r--   17  9 Feb 11:54 │  ├──  COMMIT_EDITMSG
.rw-r--r--  432  9 Feb 14:47 │  ├──  config
.rw-r--r--   73  8 Feb 22:30 │  ├──  description
.rw-r--r--   97  9 Feb 14:48 │  ├──  FETCH_HEAD
.rw-r--r--   21  9 Feb 12:10 │  ├──  HEAD
.rw-r--r-- 1.8k  9 Feb 14:47 │  ├──  index
.rw-r--r--   41  9 Feb 12:10 │  ├──  ORIG_HEAD
.rw-r--r--   46  9 Feb 12:03 │  └──  packed-refs
drwxr-xr-x    -  9 Feb 14:47 ├──  dotfiles
drwxr-xr-x    -  9 Feb 12:10 │  ├──  apps
drwxr-xr-x    -  9 Feb 12:10 │  ├──  shell
drwxr-xr-x    -  9 Feb 12:10 │  ├──  tools
.rw-r--r--  787  9 Feb 12:10 │  └──  install.conf.yaml
.rw-r--r--  842  9 Feb 12:10 ├──  .gitlab-ci.yml
.rw-r--r--  137  9 Feb 14:47 └──  .gitmodules

```

We already have a folder `dotfiles`, submodule `dotbot` needs to go in that folder
```sh-session
git submodule add https://github.com/anishathalye/dotbot dotfiles/dotbot
```
```
Cloning into '~/Bootstrap/dotfiles/dotbot'...
remote: Enumerating objects: 1519, done.
remote: Counting objects: 100% (331/331), done.
remote: Compressing objects: 100% (142/142), done.
remote: Total 1519 (delta 186), reused 262 (delta 172), pack-reused 1188
Receiving objects: 100% (1519/1519), 349.12 KiB | 4.16 MiB/s, done.
Resolving deltas: 100% (882/882), done.

```

```sh-session
❯ git config -f .gitmodules submodule.dotbot.ignore dirty

```

```sh-session
cd dotfiles
cp dotbot/tools/git-submodule/install .
```


```sh=session
exa ~/Bootstrap -all --long --icons --no-user --tree --level=2
```

```
drwxr-xr-x    -  9 Feb 15:01  ~/Bootstrap
drwxr-xr-x    -  9 Feb 14:47 ├──  .git
.rw-r--r--   41  9 Feb 12:10 │  ├──  AUTO_MERGE
.rw-r--r--   17  9 Feb 11:54 │  ├──  COMMIT_EDITMSG
.rw-r--r--  432  9 Feb 14:47 │  ├──  config
.rw-r--r--   73  8 Feb 22:30 │  ├──  description
.rw-r--r--   97  9 Feb 14:48 │  ├──  FETCH_HEAD
.rw-r--r--   21  9 Feb 12:10 │  ├──  HEAD
drwxr-xr-x    -  8 Feb 22:30 │  ├──  hooks
.rw-r--r-- 1.8k  9 Feb 14:47 │  ├──  index
drwxr-xr-x    -  8 Feb 22:30 │  ├──  info
drwxr-xr-x    -  9 Feb 11:53 │  ├──  logs
drwxr-xr-x    -  9 Feb 14:47 │  ├──  modules
drwxr-xr-x    -  9 Feb 14:48 │  ├──  objects
.rw-r--r--   41  9 Feb 12:10 │  ├──  ORIG_HEAD
.rw-r--r--   46  9 Feb 12:03 │  ├──  packed-refs
drwxr-xr-x    -  9 Feb 11:12 │  └──  refs
.rw-r--r--  842  9 Feb 12:10 ├──  .gitlab-ci.yml
.rw-r--r--  137  9 Feb 14:47 ├──  .gitmodules
drwxr-xr-x    -  9 Feb 16:01 ├──  dotfiles
drwxr-xr-x    -  9 Feb 12:10 │  ├──  apps
drwxr-xr-x    -  9 Feb 14:47 │  ├──  dotbot
.rwxr-xr-x  368  9 Feb 16:01 │  ├──  install
.rw-r--r--  787  9 Feb 12:10 │  ├──  install.conf.yaml
drwxr-xr-x    -  9 Feb 12:10 │  ├──  shell
drwxr-xr-x    -  9 Feb 12:10 │  └──  tools
.rw-r--r-- 2.2k  9 Feb 15:01 └──  README.md

```
