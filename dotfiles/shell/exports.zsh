#!/bin/sh

HISTSIZE=1000000
SAVEHIST=1000000
setopt INC_APPEND_HISTORY
setopt EXTENDED_HISTORY
export HISTTIMEFORMAT="[%F %T] "

export LC_CTYPE=nl_BE.UTF-8

# XDG Paths
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share

# Environment variables set everywhere
export EDITOR="nvim"
export TERM="xterm-256color"
export TERMINAL='alacritty'

export PATH="$HOME/.local/bin":$PATH
export MANPAGER='vim +Man!'
export MANWIDTH=999
export PATH=$HOME/.cargo/bin:$PATH
export PATH=$HOME/.local/share/go/bin:$PATH
export GOPATH=$HOME/.local/share/go
export PATH=$HOME/.fnm:$PATH
# export PATH="$HOME/.local/share/neovim/bin":$PATH

export HOMEBREW_NO_GOOGLE_ANALYTICS=1

# export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin:/usr/local/git/bin:/usr/local/MacGPG2/bin"

export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
# eval "$(pyenv virtualenv-init -)"
# export PIPENV_VERBOSITY=-1

eval "$(starship init zsh)"
